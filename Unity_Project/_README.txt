The year is 1592 and the Japanese have launched an invasion of Korea. Cursed with corruption and incompetent leaders, Korea has all but fallen, their armies overwhelmed and navy destroyed. Korea's fate now lies in a handful of ships and Admiral Yi Sun-sin, a man who has never fought a naval battle.

Play as the greatest naval commander of all time, Admiral Yi Sun-sin, as you defend Korea. Features a massive open world accurately modelled on the Korea coast.

Use the arrow keys to manoeuvre your ship and click to fire your cannons. Use control and shift to raise and lower your sails and catch the wind. Press "m" to view the map and plan your battle. Dock with the harbour at Yeosu to repair, build and upgrade ships.


=== NOTES ===
Used original project for Grads in Games competition, but extremely heavily modified.
I copied this project between several computers so have uploaded it to a new Bitbucket repo
All additional code is my own

Models constructed using the Polygon Samurai pack by SYNTY studios.

Royalty free music reference:

Japanese TAIKO drum Copyright free music | FMB
https://www.youtube.com/watch?v=M8VvZObWtZM
Enjoy music!  by FreeMusicBox?
[MUSIC]
�SHW - Tsudzumi JAPAN 3
�http://shw.in/sozai/japan.php
 * 
Mindkeys - Ascension [Royalty Free]
https://www.youtube.com/watch?v=hFEzVSYdXKw&index=10&list=RDyAHgqDPfaLM
This song is royalty free so please be free to use it
Artist: Mindkeys
Title: Ascension
Genre: Chillstep
Software: FL Studio 11
