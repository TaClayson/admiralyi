﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Opening : MonoBehaviour {

    public static bool active = true;

    public CanvasGroup CG;
    public CanvasGroup nextImageCG;
    public CanvasGroup subtitleCG;
    public Text subtitles;
    public RawImage currentImage;
    public RawImage nextImage;
    public AudioSource audio;
    public MeshRenderer map;

    bool showImages = false;
    bool showMap = true;
    bool showNextImage = false;
    bool showSubtitle = true;
    bool battleMusic = false;

    int stage = 0;
    float fadeSpeed = 1;

    public Texture2D peace;
    public Texture2D Toyotomi;
    public Texture2D landing;
    public Texture2D castle;
    public Texture2D Yi;

    float coolDown = 0;

    private void Awake() {
        map.gameObject.SetActive(true);
		RenderSettings.fog = false;
    }

    void Update () {
        //show map
        float alpha = map.material.color.a;
        if (showMap) {
            alpha = Mathf.Clamp01( alpha+Time.deltaTime/fadeSpeed );
        } else {
            alpha = Mathf.Clamp01( alpha-Time.deltaTime/fadeSpeed );
        }
        map.material.color = new Color(1,1,1,alpha);

        //show intro images
        if (showImages) {
            CG.alpha = Mathf.Clamp01( CG.alpha+Time.deltaTime/fadeSpeed );
        } else {
            CG.alpha = Mathf.Clamp01( CG.alpha-Time.deltaTime/fadeSpeed );
        }

        //move images
        Vector3 pos = nextImage.rectTransform.localPosition;
        nextImage.rectTransform.localPosition = pos-Vector3.up*Time.deltaTime*10;
        pos = currentImage.rectTransform.localPosition;
        currentImage.rectTransform.localPosition = pos-Vector3.up*Time.deltaTime*10;

        //show next image
		if (showNextImage) {
            nextImageCG.alpha = Mathf.Clamp01( nextImageCG.alpha+Time.deltaTime/fadeSpeed );
            if (nextImageCG.alpha == 1) {
                currentImage.gameObject.SetActive(true);
                currentImage.texture = nextImage.texture;
                currentImage.rectTransform.sizeDelta = nextImage.rectTransform.sizeDelta;
                currentImage.rectTransform.localPosition = nextImage.rectTransform.localPosition;
                showNextImage = false;
                nextImageCG.alpha = 0;
            }
        } else {
            nextImageCG.alpha = Mathf.Clamp01( nextImageCG.alpha-Time.deltaTime/fadeSpeed );
        }

        //show subtitles
        if (showSubtitle) {
            subtitleCG.alpha = Mathf.Clamp01( subtitleCG.alpha+Time.deltaTime/fadeSpeed );
        } else {
            subtitleCG.alpha = Mathf.Clamp01( subtitleCG.alpha-Time.deltaTime/fadeSpeed );
        }

        //battle music
        if (battleMusic) {
            MusicManager.Ibattle += (5-MusicManager.Ibattle)*Time.deltaTime/5f;
        }

        //trigger next stage
        coolDown -= Time.deltaTime;
        if (coolDown < 0) {
            coolDown = 3;
            NextStage();
        }

        //cancel
        if (Input.GetKey(KeyCode.Escape)) {
            showImages = false;
            showMap = false;
            showNextImage = false;
            battleMusic = false;
            showSubtitle = false;
            active = false;
			RenderSettings.fog = true;
            subtitles.text = "";
            Player.currentState = Player.States.Ship;
        }
	}

    void NextStage() {
        if (!active) {
            return;
        }

        nextImage.rectTransform.localPosition = new Vector3(0,100,0);

        switch (stage) {

            case 0:
                showMap = true;
                showImages = false;
                showNextImage = false;
                subtitles.text = "Korea 1592";
                coolDown = 5;
                break;

            case 1:
                showImages = true;
                showNextImage = true;
                nextImage.texture = peace;
                nextImage.rectTransform.sizeDelta = new Vector2(1000,1000*1688/1206);
                subtitles.text = "The Joseon dynasty ruled a peaceful nation. However, this peace had " +
                    "let corruption run rampant through the court.";
                coolDown = 10;
                break;

            case 2:
                showNextImage = true;
                nextImage.texture = Toyotomi;
                nextImage.rectTransform.sizeDelta = new Vector2(1000,1000*783/700);
                subtitles.text = "Japan, now unified under Toyotomi, and wishing to expand their empire " +
                    "westward prepared their forces to invade Korea.";
                coolDown = 10;
                break;

            case 3:
                showNextImage = true;
                battleMusic = true;
                nextImage.texture = landing;
                nextImage.rectTransform.sizeDelta = new Vector2(1000,1000*715/542);
                subtitles.text = "They came in the their thousands. Hundreds of boats unloaded their " +
                    "troops along the beach with no one to opose them.";
                coolDown = 10;
                break;

            case 4:
                showNextImage = true;
                nextImage.texture = castle;
                nextImage.rectTransform.sizeDelta = new Vector2(1000,1000*800/547);
                subtitles.text = "Their invasion was swift and the Korean forces and fortifications were " +
                    "all but useless.";
                coolDown = 10;
                break;

            case 5:
                showNextImage = true;
                nextImage.texture = Yi;
                nextImage.rectTransform.sizeDelta = new Vector2(1000,1000*550/420);
                subtitles.text = "Defeat was all but certain. Koreas only hope, the remanants of a once great " +
                    "fleet and an admiral who had never fought at sea before.";
                coolDown = 10;
                break;

            case 6:
                showImages = false;
                subtitles.text = "Admiral Yi Sun-sin.";
                coolDown = 3;
                break;

            case 7:
                showMap = false;
                battleMusic = false;
                showSubtitle = false;
                subtitles.text = "";
                coolDown = 2;
                break;

            case 8:
                Player.currentState = Player.States.Ship;
                active = false;
				RenderSettings.fog = true;
                break;

        }
        stage++;

    }
}