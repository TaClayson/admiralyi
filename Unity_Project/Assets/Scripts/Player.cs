﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static float honour = 0;
    public static int cannon = 1;
    public static int armour = 1;
    public static int speed = 1;

    public static Dock dock;

    public enum States { Person, Ship, Command, Map, HighMap };
    public static States currentState = States.HighMap;

    public static Player local;
    public Transform respawn;

    float cameraAx = 50;
    float cameraAy = -50;
    float zoom = 20000;

    void Awake() {
        local = this;
    }

    private void LateUpdate() {
        if (dock!=null){
            Debug.Log(dock.name);
        }

        //enable disable map
        if (currentState==States.Map) {
            Map.local.gameObject.SetActive(true);
        } else {
            Map.local.gameObject.SetActive(false);
        }

        //enable disable clouds
        if (currentState==States.HighMap) {
            CloudSpawn.local.gameObject.SetActive(false);
        } else {
            CloudSpawn.local.gameObject.SetActive(true);
        }

        switch ( currentState ) {
            case States.Ship:
                zoom += (100-zoom)*Time.deltaTime;
                if (Input.GetButtonDown("Map")) {
                    currentState = States.Map;
                }
                break;

            case States.Map:
                zoom += (5000-zoom)*Time.deltaTime;
                if (Input.GetButtonDown("Map")) {
                    currentState = States.Ship;
                }
                break;

            case States.HighMap:
                zoom += (20000-zoom)*Time.deltaTime;
                break;
        }

        //camera
        if (currentState==States.HighMap) {
            cameraAx = 50;
            cameraAy += Time.deltaTime*10;
        } else {
            CameraControl();
        }

         //position camera
        transform.position = ShipController.ship.transform.position;
        transform.rotation = Quaternion.Euler(cameraAx,cameraAy,0);
        transform.Translate(0,0,-zoom);
    }

    void CameraControl() {
        //control camera
        cameraAx -= Input.GetAxis("Mouse Y");
        cameraAy += Input.GetAxis("Mouse X");
        cameraAx = Mathf.Clamp(cameraAx,20,80);
        cameraAy = cameraAy%360;
    }

}