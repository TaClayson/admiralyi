﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetMgr : MonoBehaviour {

    public static AssetMgr local;

    public GameObject explosion;
    public GameObject splash;
    public GameObject healing;

    public GameObject koreanSmallShip;
    public GameObject koreanLargeShip;
    public GameObject koreanTurtleShip;
    public GameObject japaneseSmallShip;
    public GameObject japaneseLargeShip;

    public GameObject[] buildings;
    public Transform cityParent;

    private void Awake() {
        local = this;
    }

    public static void SpawnBuildings(int number, Vector3 centre, float radius) {
        for(int i=0; i<number; i++){
            Vector3 pos = centre;
            bool validPos = false;
            int attempts = 100;
            while(!validPos && attempts>0){
                attempts--;
                pos = centre + Quaternion.Euler(0,Random.value*360f,0)
                                *Vector3.right*radius*Mathf.Sqrt(Random.value);
                RaycastHit hit;
                if (Physics.Raycast(pos+Vector3.up*1000, Vector3.down, out hit)) {
                    if (hit.collider!=null){
                        if (hit.collider.gameObject.GetComponent<Terrain>()!=null) {
                            pos = hit.point;
                            validPos = true;
                        }
                    }
                }
            }
            if (validPos){
                GameObject newBuild = Instantiate(local.buildings[ Random.Range(0, local.buildings.Length) ]);
                newBuild.transform.parent = local.cityParent;
                newBuild.transform.position = pos;
                newBuild.transform.rotation = Quaternion.Euler(0,Random.value*360f,0);
                newBuild.transform.localScale = new Vector3( Random.Range(1f,2f),Random.Range(1f,2f),Random.Range(1f,2f) );
            }
        }
    }

    public static GameObject SpawnShips(int faction, int large, int small, Vector3 centre, float radius) {
        //return leader ship
        GameObject leadShip = null;

        //spawn large ships
        for(int i=0; i<large; i++){
            GameObject newShip = null;
            if (faction==0){
                newShip = Instantiate(local.koreanLargeShip);
            } else {
                newShip = Instantiate(local.japaneseLargeShip);
            }
            if (leadShip == null) {
                leadShip = newShip;
            }
            Vector3 pos = centre + Quaternion.Euler(0,Random.value*360f,0)
                *Vector3.right*radius*Mathf.Sqrt(Random.value);
            newShip.transform.position = pos;
            newShip.GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(pos);
            newShip.GetComponent<ShipAI>().currentState = ShipAI.States.Defend;
            newShip.GetComponent<ShipAI>().defendTarget = leadShip;
        }

        //spawn small ships
        for(int i=0; i<small; i++){
            GameObject newShip = null;
            if (faction==0){
                newShip = Instantiate(local.koreanSmallShip);
            } else {
                newShip = Instantiate(local.japaneseSmallShip);
            }
            if (leadShip == null) {
                leadShip = newShip;
            }
            Vector3 pos = centre + Quaternion.Euler(0,Random.value*360f,0)
                *Vector3.right*radius*Mathf.Sqrt(Random.value);
            newShip.transform.position = pos;
            newShip.GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(pos);
            newShip.GetComponent<ShipAI>().currentState = ShipAI.States.Defend;
            newShip.GetComponent<ShipAI>().defendTarget = leadShip;
        }

        return leadShip;
    }

}