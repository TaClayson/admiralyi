﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIminimap : MonoBehaviour {

    //UI elements
    public Text windText;
    public RectTransform windDirection;

    public Text missionText;

	void Update () {
		
        //update wind information
        windText.text = Mathf.Round(Weather.windDirection.magnitude).ToString()+" m/s";
        Vector3 camDirection = Camera.main.transform.forward;

		float windAngle = 0;
		if (Vector3.Cross (Weather.windDirection, camDirection).y > 0) {
			windAngle = Vector3.Angle (Weather.windDirection, camDirection);
		} else {
			windAngle = -Vector3.Angle (Weather.windDirection, camDirection);
		}

        windDirection.rotation = Quaternion.Euler(0,0, windAngle+180);

        //set mission text
        missionText.text = Master.currentMission;
	}
}