﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weather : MonoBehaviour {

    public static Vector3 windDirection = Vector3.zero;
    Vector3 newWindDirection = Vector3.zero;
    float windUpdateCooldown = 0;

    private void Awake() {
        windDirection = Quaternion.Euler(0,Random.value*360,0)*Vector3.forward * Random.Range(20f,30f);
        newWindDirection = windDirection;
        windUpdateCooldown = Random.value*100;
    }

    void Update () {
        //randomly adjust the wind power and direction
        windUpdateCooldown-=Time.deltaTime;
        if (windUpdateCooldown < 0) {
            newWindDirection = Quaternion.Euler(0,Random.value*360,0)*Vector3.forward * Random.Range(20f,30f);
            windUpdateCooldown = Random.value*100;
        }
        windDirection += (newWindDirection-windDirection)*Time.deltaTime;
	}
}