﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

    //cannon stats
    public float damage = 0.1f;
    public float firePeriod = 1;        //cooldown between shots in s
    public float fireVelocity = 100;    //shot velocity in m/s
    public float fireSpread = 1;        //degree variation of shot
    public float spread = 20;
    public GameObject cannonBallPrefab;

    public Transform nozel;
    public ParticleSystem fireEffect;
    float fireCooldown = 0;

	void Update () {
		fireCooldown -= Time.deltaTime;
    }

    public void Fire( Vector3 inheritVelocity ){
        if (fireCooldown > 0) {
            return;
        }
        fireEffect.Emit(50);
        GameObject newBall = Instantiate(cannonBallPrefab);
        newBall.transform.position = nozel.transform.position + nozel.transform.forward;
        newBall.transform.rotation = nozel.transform.rotation
            * Quaternion.Euler(Random.Range(-spread,spread),Random.Range(-spread,spread),Random.Range(-spread,spread));
        newBall.GetComponent<Rigidbody>().velocity = newBall.transform.forward * fireVelocity + inheritVelocity;
        fireCooldown = firePeriod*Random.Range(0.5f,2f);
        newBall.GetComponent<CannonBall>().damage = damage;
    }
}