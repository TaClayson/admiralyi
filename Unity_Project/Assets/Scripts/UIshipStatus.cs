﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIshipStatus : MonoBehaviour {

    //UI elements
    public Text healthText;
    public Text speedText;
    public Transform sailsBars;
    public RectTransform healthBar;

	void Update () {
		
        //health
        healthText.text = Mathf.Round(ShipController.ship.health*100).ToString()+"%";
        healthBar.anchorMax = new Vector2(ShipController.ship.health,1);

        //set speed text
        float speed = ShipController.ship.GetComponent<Rigidbody>().velocity.magnitude;
        speedText.text = "Speed: "+Mathf.Round(speed).ToString()+" m/s";

        //set sails bars
        int bars = Mathf.RoundToInt( ShipController.ship.sailsPower * sailsBars.childCount );
        for(int i=0; i<sailsBars.childCount; i++) {
            if (i<bars){
                sailsBars.GetChild(i).gameObject.SetActive(true);
            } else {
                sailsBars.GetChild(i).gameObject.SetActive(false);
            }
        }

    }
}