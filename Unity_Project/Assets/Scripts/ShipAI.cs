﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShipAI : MonoBehaviour {

    public GameObject defendTarget = null;
    public GameObject attackTarget = null;
    public Vector3 posTarget = Vector3.zero;
    Ship myShip;
    Vector3 offset = Vector3.zero;

    public float range = 100;

    public enum States { Idle, Hunt, Retreat, Defend };
    public States currentState = States.Defend;

    void Awake() {
        myShip = GetComponent<Ship>();
        offset = new Vector3(Random.Range(-100f,100f),0,Random.Range(-100f,100f));
    }

    private void Start() {
        posTarget = transform.position;

        //grab offset if already in formation
        if (defendTarget!=null){
            if ((defendTarget.transform.position - transform.position).sqrMagnitude < range * range) {
                offset = Quaternion.Inverse(defendTarget.transform.rotation) *
                        (transform.position - defendTarget.transform.position);
            }
        }
    }

    void Update () {

        FindTarget();

        switch (currentState) {

            case States.Hunt:
                AttackTarget();
                break;

            case States.Defend:
                if (defendTarget != null) {
                    posTarget = defendTarget.transform.position + defendTarget.transform.rotation*offset;
                }

                if (attackTarget!=null){
                    if ((attackTarget.transform.position-transform.position).sqrMagnitude<range*range){
                        AttackTarget();
                    } else {
                        GoTo(posTarget);
                    }
                } else {
                    GoTo(posTarget);
                }
                break;

        }

    }

    void FindTarget(){
        //find a target
        attackTarget = null;
        float closest = 1e10f;
        foreach(GameObject obj in Master.shipArray) {
            if (obj != gameObject && obj!=null) {
                Ship ship = obj.GetComponent<Ship>();
                if (ship != null) {

                    if (ship.faction!=myShip.faction && ship.health>0){

                        if ((ship.transform.position - transform.position).sqrMagnitude < closest) {
                            closest = (ship.transform.position - transform.position).sqrMagnitude;
                            attackTarget = obj;
                        }

                    }

                }
            }
        }
	}

    void GoTo(Vector3 position) {
        Vector3 trueDirect = position - transform.position;
        trueDirect.y = 0;

        //uses navmesh to get headings and stuff
        NavMeshAgent nma = GetComponent<NavMeshAgent>();
        if (nma == null) {
            return;
        }
        nma.SetDestination(position);
        if (nma.path.corners.Length <= 1) {
            //already at destination
            myShip.SetSails(0);
            myShip.SetRowing(0);
            myShip.SetTurn(0);
            return;
        }
        Vector3 direct = (nma.path.corners[1] - transform.position)*100;

        if (trueDirect.sqrMagnitude>100*100){
            if (Vector3.Dot(direct,transform.right)>0){
                myShip.SetTurn( 1 );
            } else {
                myShip.SetTurn( -1 );
            }
            float speed = Vector3.Dot(direct,transform.forward);
            myShip.SetSails(speed);
            myShip.SetRowing(speed);
        } else {
            myShip.SetSails(0);
            myShip.SetRowing(0);
            myShip.SetTurn(0);
        }
    }

    void AttackTarget() {
        if (attackTarget == null) {
            return;
        }

        Vector3 direct = attackTarget.transform.position - transform.position + offset;
        direct.y = 0;
        
        //move to
        if (direct.sqrMagnitude>200*200){
            GoTo(attackTarget.transform.position);
        } else {

            Vector3 directN = (attackTarget.transform.position - transform.position).normalized;
            myShip.SetSails(0);
            myShip.SetRowing(0);
            if (Vector3.Dot(directN,transform.forward)>0){
                myShip.SetTurn( 1 );
            } else {
                myShip.SetTurn( -1 );
            }

            //fire broadsides
            if (Vector3.Dot(directN, transform.forward) < 0.2f) {
                myShip.FireCannons(directN);
            }

        }

        
    }

}