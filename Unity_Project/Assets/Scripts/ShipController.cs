﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

    public Ship initShip;
    public static Ship ship;

    private void Start() {
        ship = initShip;
        ship.faction = Master.factions[0];  //set player ship to korean faction
        ship.GetComponent<ShipAI>().enabled = false;
    }

    void Update () {
        ship.faction = Master.factions[0];  //set player ship to korean faction

        // Get Player's movement input and determine direction and set run speed
        float turnInput = Input.GetAxisRaw("Horizontal");
        ship.SetTurn(turnInput);

        float rowInput = Input.GetAxisRaw("Vertical");
        ship.SetRowing(rowInput);

        if (Input.GetButtonDown("Faster")) {
            ship.IncrementSails(0.2f);
        }
        if (Input.GetButtonDown("Slower")) {
            ship.IncrementSails(-0.2f);
        }

        //FIRE!
        if (Input.GetButton("Fire1") && !UIMission.open && !UIdock.open) {
            ship.FireCannons(transform.forward);
        }

    }
}