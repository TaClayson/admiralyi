﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour {

    public float damage = 1f;

	void Start(){
		GetComponent<AudioSource>().pitch = Random.Range(0.8f,1.2f);
	}

    void OnCollisionEnter(Collision collision){

        //explosion
        GameObject newExp = Instantiate(AssetMgr.local.explosion);
        newExp.transform.position = transform.position;
        Destroy(newExp,2);

        //damage ships if we hit them
        Ship ship = collision.transform.root.GetComponent<Ship>();
        if (ship != null) {
            ship.Damage(damage);
        }

        Destroy(gameObject,0.5f);
    }

    void OnTriggerEnter(Collider collision){

        //splash
        GameObject newExp = Instantiate(AssetMgr.local.splash);
        newExp.transform.position = transform.position;
        Destroy(newExp,2);

        Destroy(gameObject,0.5f);
    }
}