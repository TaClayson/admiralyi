﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text honourText;
    public GameObject mainUI;

    private void Update() {
        if (Opening.active) {
            mainUI.SetActive(false);
        } else {
            mainUI.SetActive(true);
        }

        honourText.text = "Honour: "+Mathf.Round(Player.honour).ToString();
    }

    public void MissionButton() {
        UIMission.Enable();
    }
}
