﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    public Faction faction;

    //ship movement attributes - all are physics based so are non-linear
    public float rowingSpeed = 500;
    public float sailingSpeed = 100;
    public float manuverability = 1000;
    public float armor = 10;
    public int factionN = 0;
    public float honour = 20;

    //what the ship is currently doing
    public float rowPower = 0;  //clamped between -0.5 and 1 (can row backwards at half speed)
    public float sailsPower = 0; //clamped between 0 and 1 (can't sail backwards)

    public float health = 1;

    Rigidbody rb;

    List<Cannon> cannons = new List<Cannon>();
    public GameObject healingEffect = null;

    private void Start() {
        rb = GetComponent<Rigidbody>();

        faction = Master.factions[factionN];

        //find cannons
        foreach(Transform child in transform) {
            if (child.GetComponent<Cannon>() != null) {
                cannons.Add(child.GetComponent<Cannon>());
            }
        }
    }

    public void Update() {

        if (health <= 0) {
            //sink animation
            if (GetComponent<UnityEngine.AI.NavMeshAgent>().enabled){
                transform.position = new Vector3(transform.position.x,0,transform.position.z);
                GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
            }
            transform.position -= Vector3.up*Time.deltaTime*3;
            if (transform.position.y < -15) {

                if (ShipController.ship == this) {
                    //respawn player
                    health = 1;
                    transform.position = Player.local.respawn.position;
                    transform.rotation = Player.local.respawn.rotation;
                }else{
                    Destroy(gameObject);
                }

                //gain honour when
                if (faction == Master.factions[1]) {
                    Player.honour += honour;
                }

            }
            return;
        }

        //rowing force
        rowPower = Mathf.Clamp(rowPower,-0.5f,1f);
        if (faction==Master.factions[0]){
            rowPower *= 1+(float)Player.speed/10; //apply research
        }
        rb.AddForce( transform.forward * rowingSpeed * rowPower );

        //sailing force
        sailsPower = Mathf.Clamp01(sailsPower);
        if (faction==Master.factions[0]){
            sailsPower *= 1+(float)Player.speed/10; //apply research
        }
        float windPower = Vector3.Dot(transform.forward, Weather.windDirection);
        if (windPower < 0) {
            windPower = 0;
        }
        rb.AddForce( transform.forward * sailingSpeed * sailsPower * windPower );

        //fix constraits
        transform.rotation = Quaternion.Euler( 0, transform.rotation.eulerAngles.y, 0 );
        transform.position = new Vector3(transform.position.x,0,transform.position.z);

    }

    public void SetTurn(float angle) {
        if (health <= 0) {
            return;
        }
        //turn a ship in a specified direction, angle is clamped between -1 and 1
        angle = Mathf.Clamp(angle,-1f,1f);
        float torque = angle * manuverability;
        rb.AddTorque(0,torque,0);
    }

    public void SetHeading(float angle) {
        //slowly turns ship towards angle
        angle -= transform.rotation.eulerAngles.y;
        SetTurn(angle);
    }

    public void SetRowing(float rowing) {
        rowPower = rowing;
    }

    public void SetSails(float value) {
        sailsPower = value;
    }
    public void IncrementSails(float value) {
        SetSails( sailsPower + value );
    }


    public void FireCannons( Vector3 direction ) {
        if (health <= 0) {
            return;
        }
        //fires cannons pointing in this general direction
        foreach(Cannon can in cannons) {
            if (Vector3.Dot(can.transform.forward, direction) > 0.5f) {
                can.Fire( rb.velocity );
            }
        }
    }

    public void Damage(float damage) {
        //apply research
        if (faction==Master.factions[0]){
            damage /= 1+(float)Player.armour/10;
        }
        if (faction==Master.factions[1]){
            damage *= 1+(float)Player.armour/10; 
        }
        health -= damage/armor;
    }

    public void OnTriggerEnter(Collider other) {
        //docks
        if (other.GetComponent<Dock>() != null) {
            if (ShipController.ship == this) {
                if (!UIdock.open){
                    UIdock.Enable();
                }
                Player.dock = other.GetComponent<Dock>();
            }
        }
    }

    public void OnTriggerStay(Collider other) {
        //healing
        if (other.GetComponent<Healing>() != null) {
            health += Time.deltaTime;
            health = Mathf.Clamp01(health);
            if (healingEffect == null) {
                healingEffect = Instantiate(AssetMgr.local.healing);
                healingEffect.transform.parent = transform;
                healingEffect.transform.localPosition = Vector3.zero;
            }
        }
    }

    public void OnTriggerExit(Collider other) {
        //healing
        if (other.GetComponent<Healing>() != null) {
            if (healingEffect != null) {
                Destroy(healingEffect,5);
            }
        }
    }

}