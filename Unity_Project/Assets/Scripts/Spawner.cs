﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public int faction = 0;
    public int large = 0;
    public int small = 0;

	void Update () {
        if (ShipController.ship == null) {
            return;
        }

        if (Master.CountShips( Master.factions[faction], transform.position, 1000)==0){
            if ((ShipController.ship.transform.position - transform.position).sqrMagnitude>1000*1000) {
                AssetMgr.SpawnShips(faction,large,small, transform.position, 100);
            }
        }
		
	}
}