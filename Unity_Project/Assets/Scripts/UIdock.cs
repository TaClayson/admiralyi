﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIdock : MonoBehaviour {

    public static bool open = false;
    public static UIdock local;

    public Text cannonsCost;
    public Text armourCost;
    public Text speedCost;

    void Awake() {
		local = this;
        open = true;
        Disable();
	}

    void Update() {
        cannonsCost.text = ResearchCost(Player.cannon).ToString()+" honour";
        armourCost.text = ResearchCost(Player.armour).ToString()+" honour";
        speedCost.text = ResearchCost(Player.speed).ToString()+" honour";
    }

    public void CloseButton() {
        Disable();
    }

    public static void Enable() {
        if (open) {
            Disable();
            return;
        }
        foreach(Transform child in local.transform) {
            child.gameObject.SetActive(true);
        }
        open = true;
    }

    public static void Disable() {
        if (!open) {
            Enable();
            return;
        }
        foreach(Transform child in local.transform) {
            child.gameObject.SetActive(false);
        }
        open = false;
    }

    public void Construct(string name) {
        GameObject prefab = null;
        float cost = 0;
        if (name == "small") {
            prefab = AssetMgr.local.koreanSmallShip;
            cost = 100;
        }
        if (name == "large") {
            prefab = AssetMgr.local.koreanLargeShip;
            cost = 200;
        }
        if (name == "turtle") {
            prefab = AssetMgr.local.koreanTurtleShip;
            cost = 150;
        }

        if (Player.honour<cost){
            return;
        }
        Player.honour-=cost;
        GameObject newObj = Instantiate(prefab);
        newObj.transform.position = Player.dock.spawnPoint.position
                        + Quaternion.Euler(0,Random.value*360f,0)*Vector3.forward*Random.value*100f;
        newObj.GetComponent<ShipAI>().defendTarget = ShipController.ship.gameObject;
        newObj.GetComponent<Ship>().faction = Master.factions[0];
    }

    public void Research(string name) {
        float cost = 0;
        if (name == "cannons") {
            cost =  ResearchCost(Player.cannon);
        }
        if (name == "armour") {
            cost =  ResearchCost(Player.armour);
        }
        if (name == "speed") {
            cost =  ResearchCost(Player.speed);
        }
        if (Player.honour < cost) {
            return;
        }
        Player.honour -= cost;
        if (name == "cannons") {
            Player.cannon++;
        }
        if (name == "armour") {
            Player.armour++;
        }
        if (name == "speed") {
            Player.speed++;
        }
    }

    float ResearchCost(int level) {
        return level*200;
    }

}