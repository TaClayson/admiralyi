﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMission : MonoBehaviour {

    public static bool open = false;
    public static UIMission local;

    //UI elements
    public Text titleText;
    public Text bodyText;

	void Awake() {
		local = this;
        open = true;
        Disable();
	}

    void Update() {
        titleText.text = Master.currentMission;
        bodyText.text = Master.missionText;
    }

    public void CloseButton() {
        Disable();
    }

    public static void Enable() {
        if (open) {
            Disable();
            return;
        }
        foreach(Transform child in local.transform) {
            child.gameObject.SetActive(true);
        }
        open = true;
    }

    public static void Disable() {
        if (!open) {
            Enable();
            return;
        }
        foreach(Transform child in local.transform) {
            child.gameObject.SetActive(false);
        }
        open = false;
    }
}