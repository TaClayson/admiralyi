﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawn : MonoBehaviour {

    public static CloudSpawn local;

	public GameObject[] cloudPrefabs;
	public int number = 20;
	public float range = 1000;
    public float height = 500;
    public float size = 10;
    public float speed = 5;

    private void Awake() {
        local = this;
    }

    void Start () {

		for (int i = 0; i < number; i++) {

			GameObject newCloud = Instantiate(cloudPrefabs[Random.Range(0,cloudPrefabs.Length)]);
            newCloud.transform.parent = transform;
			newCloud.transform.position = new Vector3 (Random.Range (-range, range), Random.Range (height, height*5), Random.Range (-range, range));
			newCloud.transform.localScale = new Vector3 (Random.Range (1f, 2f), Random.Range (1f, 2f), Random.Range (1f, 2f)) * size;
			newCloud.transform.rotation = Quaternion.Euler (0, Random.value * 360f, 0);
			newCloud.GetComponent<Cloud>().velocity = new Vector3 (Random.Range (-1f, 1f), 0, Random.Range (-1f, 1f))*speed;

		}
		
	}

}