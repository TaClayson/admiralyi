﻿/* music source and copyright
 * 
 * Japanese TAIKO drum Copyright free music | FMB
 * https://www.youtube.com/watch?v=M8VvZObWtZM
 * Enjoy music!  by FreeMusicBox♪
 * [MUSIC]
 * •SHW - Tsudzumi JAPAN 3
 * •http://shw.in/sozai/japan.php
 * 
 * Mindkeys - Ascension [Royalty Free]
 * https://www.youtube.com/watch?v=hFEzVSYdXKw&index=10&list=RDyAHgqDPfaLM
 * This song is royalty free so please be free to use it for your videos, please refer back to me on your video or video description.
 * Artist: Mindkeys
 * Title: Ascension
 * Genre: Chillstep
 * Software: FL Studio 11
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    //music intensities
    public static float Iadventure = 1;
    public static float Ibattle = 0;
    public static float Iawe = 0;

    public AudioSource adventure;
    public AudioSource battle;
    public AudioSource awe;

	void Update () {
		
        float totalIntensity = Iadventure+Ibattle+Iawe;
        adventure.volume = Iadventure/totalIntensity;
        battle.volume = Ibattle/totalIntensity;
        awe.volume = Iawe/totalIntensity;


        //increase battle if there are loads of enemies near the player
        float newBattle = 0;
        if (ShipController.ship != null) {
            Faction myFaction = ShipController.ship.faction;
            foreach(GameObject obj in Master.shipArray) {
                if (obj!=null){
                    if (obj.GetComponent<Ship>() != null) {
                        if (obj.GetComponent<Ship>().faction != myFaction) {
                            if ((obj.transform.position-ShipController.ship.transform.position).sqrMagnitude<300*300){
                                newBattle++;
                            }
                        }
                    }
                }
            }
        }
        Ibattle += (newBattle-Ibattle)*Time.deltaTime/5f;

	}
}