﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMarker : MonoBehaviour {

    public TextMesh text;
    public GameObject flag;

    void Update () {
		
        text.text = name;

         //rotate to face camera
        Vector3 direct = transform.position - Camera.main.transform.position;
        text.transform.rotation = Quaternion.LookRotation( direct );
        direct.y = 0;
        flag.transform.rotation = Quaternion.LookRotation( direct );

	}
}